import React, { Component, Fragment } from "react";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Link,
  Switch,
} from "react-router-dom";
import { Row, Col } from "antd";
import NavBar from "./components/NavBar";
import About from "./components/About";
import Home from "./components/Home";

class App extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <NavBar />
          <Switch>
            <Route
              exact
              path="/movies/:id"
              component={props => <Home {...props} />}
            />
            <Route
              exact
              path="/movies/:id/actor"
              component={props => <About {...props} />}
            />
          </Switch>
        </Fragment>
      </Router>
    );
  }
}

export default App;
