import React, { Component } from "react";
import { Table } from "antd";

class About extends Component {
  handleClick = event => {
    console.log(event);
  };
  render() {
    const { match, surveyList } = this.props;

    const columns = [
      {
        title: "Name",
        dataIndex: "surveyName",
        key: "4",
      },
      {
        title: "Position",
        dataIndex: "position",
        key: "3",
      },
      {
        title: "Date",
        dataIndex: "date",
        key: "2",
      },
      {
        title: "Action",
        dataIndex: "id",
        key: "1",
        render: text => <a onClick={() => this.handleClick(text)}>{text}</a>,
      },
    ];

    return (
      <div>
        <Table
          rowKey={record => record.uid}
          columns={columns}
          dataSource={surveyList}
        />
      </div>
    );
  }
}
About.defaultProps = {
  surveyList: [
    {
      id: "12",
      surveyName: "Survey 1",
      position: "T4",
      date: "17/07/2017",
    },
    {
      id: "12",
      surveyName: "Survey 89",
      position: "F8",
      date: "12/03/2016",
    },
    {
      id: "34",
      surveyName: "Survey 56",
      position: "H67",
      date: "21/01/2018",
    },
  ],
};
export default About;
