import React, { Component } from "react";

class Home extends Component {
  render() {
    const { match } = this.props;
    console.log(match);
    return (
      <div>
        Movie {match.params.id}
      </div>
    );
  }
}
export default Home;
