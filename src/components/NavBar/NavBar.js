import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const Sep = () => <span> | </span>;

const NavBar = () => {
  return (
    <Fragment>
      <Link to="/movies/12">Home</Link> <Sep />
      <Link to="/movies/12/actor">About</Link> <Sep />
    </Fragment>
  );
};

export default NavBar;
